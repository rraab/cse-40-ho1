#!/usr/bin/env python3

import pytest
import testbook as tb

exe_cells = [
    'q1a', 'q1b', 'q2_load', 'q2a', 'q2b', 'q2c', 'q2d', 'q4', 'q5a', 'q5b'
]

nb_vars = [
    'q1a', 'q1b', 'q2a', 'q2b', 'q2c', 'q2d', 'q4', 'q5a', 'q5b'
]

@pytest.fixture(scope='module')
def nb():
    with tb.testbook('ho1.ipynb', execute=exe_cells) as nb:

        yield { k: nb.ref(k).resolve() for k in nb_vars }

@pytest.mark.filterwarnings('ignore::DeprecationWarning')
def test_q1a(nb):
    print('Round answer for q1a to nearest 0.1')
    assert not (nb['q1a'] % 0.1)

def test_q1b(nb):
    print('Answer for q1b must be an integer')
    assert not (nb['q1b'] % 1)

def test_q2a(nb):
    print('Answer for q2a must be a float')
    assert isinstance(nb['q2a'], float)

def test_q2b(nb):
    print('Answer for q2b must be a float')
    assert isinstance(nb['q2b'], float)

def test_q2c(nb):
    print('Answer for q2c must be an int')
    assert isinstance(nb['q2c'], int)

def test_q2d(nb):
    print('Answer for q2d must be a float')
    assert isinstance(nb['q2d'], float)

def test_q4(nb):
    print('Round answer for q4 to nearest 10')
    assert not (nb['q4'] % 10)

def test_q5a(nb):
    print('Answer for q5a must be a boolean')
    assert isinstance(nb['q5a'], bool)

def test_q5b(nb):
    print('Answer for q5b must be a float')
    assert isinstance(nb['q5b'], float)
